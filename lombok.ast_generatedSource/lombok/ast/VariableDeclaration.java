//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class VariableDeclaration extends lombok.ast.AbstractNode implements lombok.ast.Statement, lombok.ast.TypeMember, lombok.ast.JavadocContainer {
	private lombok.ast.AbstractNode javadoc = null;
	private lombok.ast.AbstractNode definition = null;
	
	public Block upToBlock() {
		if (!(this.getParent() instanceof Block)) return null;
		Block out = (Block)this.getParent();
		if (!out.rawContents().contains(this)) return null;
		return out;
	}
	
	public TypeBody upToTypeBody() {
		if (!(this.getParent() instanceof TypeBody)) return null;
		TypeBody out = (TypeBody)this.getParent();
		if (!out.rawMembers().contains(this)) return null;
		return out;
	}
	
	
	public Comment astJavadoc() {
		if (!(this.javadoc instanceof Comment)) return null;
		return (Comment) this.javadoc;
	}
	
	public lombok.ast.VariableDeclaration astJavadoc(Comment javadoc) {
		return this.rawJavadoc(javadoc);
	}
	
	public lombok.ast.Node rawJavadoc() {
		return this.javadoc;
	}
	
	public lombok.ast.VariableDeclaration rawJavadoc(lombok.ast.Node javadoc) {
		if (javadoc == this.javadoc) return this;
		if (javadoc != null) this.adopt((lombok.ast.AbstractNode)javadoc);
		if (this.javadoc != null) this.disown(this.javadoc);
		this.javadoc = (lombok.ast.AbstractNode)javadoc;
		return this;
	}
	
	public VariableDefinition astDefinition() {
		if (!(this.definition instanceof VariableDefinition)) return null;
		return (VariableDefinition) this.definition;
	}
	
	public lombok.ast.VariableDeclaration astDefinition(VariableDefinition definition) {
		if (definition == null) throw new java.lang.NullPointerException("definition is mandatory");
		return this.rawDefinition(definition);
	}
	
	public lombok.ast.Node rawDefinition() {
		return this.definition;
	}
	
	public lombok.ast.VariableDeclaration rawDefinition(lombok.ast.Node definition) {
		if (definition == this.definition) return this;
		if (definition != null) this.adopt((lombok.ast.AbstractNode)definition);
		if (this.definition != null) this.disown(this.definition);
		this.definition = (lombok.ast.AbstractNode)definition;
		return this;
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		if (this.javadoc != null) result.add(this.javadoc);
		if (this.definition != null) result.add(this.definition);
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.javadoc == original) {
			this.rawJavadoc(replacement);
			return true;
		}
		if (this.definition == original) {
			this.rawDefinition(replacement);
			return true;
		}
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.javadoc == child) {
			this.disown((AbstractNode) child);
			this.javadoc = null;
			return true;
		}
		if (this.definition == child) {
			this.disown((AbstractNode) child);
			this.definition = null;
			return true;
		}
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitVariableDeclaration(this)) return;
		if (this.javadoc != null) this.javadoc.accept(visitor);
		if (this.definition != null) this.definition.accept(visitor);
		visitor.afterVisitVariableDeclaration(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public VariableDeclaration copy() {
		VariableDeclaration result = new VariableDeclaration();
		if (this.javadoc != null) result.rawJavadoc(this.javadoc.copy());
		if (this.definition != null) result.rawDefinition(this.definition.copy());
		return result;
	}
	
	public lombok.ast.StrictListAccessor<VariableDefinitionEntry,VariableDeclaration> getVariableDefinitionEntries() {
		return lombok.ast.VariableDeclarationTemplate.getVariableDefinitionEntries(this);
	}
	
	public lombok.ast.TypeDeclaration upUpToTypeDeclaration() {
		return lombok.ast.TypeMemberMixin.upUpToTypeDeclaration(this);
	}
	
}
