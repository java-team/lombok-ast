//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class Block extends lombok.ast.AbstractNode implements lombok.ast.Statement {
	lombok.ast.ListAccessor<Statement, lombok.ast.Block> contents = ListAccessor.of(this, Statement.class, "Block");
	
	public MethodDeclaration upToMethodDeclaration() {
		if (!(this.getParent() instanceof MethodDeclaration)) return null;
		MethodDeclaration out = (MethodDeclaration)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public Try upIfFinallyToTry() {
		if (!(this.getParent() instanceof Try)) return null;
		Try out = (Try)this.getParent();
		if (out.rawFinally() != this) return null;
		return out;
	}
	
	public InstanceInitializer upToInstanceInitializer() {
		if (!(this.getParent() instanceof InstanceInitializer)) return null;
		InstanceInitializer out = (InstanceInitializer)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public Synchronized upToSynchronized() {
		if (!(this.getParent() instanceof Synchronized)) return null;
		Synchronized out = (Synchronized)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public Try upIfTryBodyToTry() {
		if (!(this.getParent() instanceof Try)) return null;
		Try out = (Try)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public ConstructorDeclaration upToConstructorDeclaration() {
		if (!(this.getParent() instanceof ConstructorDeclaration)) return null;
		ConstructorDeclaration out = (ConstructorDeclaration)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public Catch upToCatch() {
		if (!(this.getParent() instanceof Catch)) return null;
		Catch out = (Catch)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public StaticInitializer upToStaticInitializer() {
		if (!(this.getParent() instanceof StaticInitializer)) return null;
		StaticInitializer out = (StaticInitializer)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public Switch upToSwitch() {
		if (!(this.getParent() instanceof Switch)) return null;
		Switch out = (Switch)this.getParent();
		if (out.rawBody() != this) return null;
		return out;
	}
	
	public Block upToBlock() {
		if (!(this.getParent() instanceof Block)) return null;
		Block out = (Block)this.getParent();
		if (!out.rawContents().contains(this)) return null;
		return out;
	}
	
	
	public lombok.ast.RawListAccessor<Statement, lombok.ast.Block> rawContents() {
		return this.contents.asRaw();
	}
	
	public lombok.ast.StrictListAccessor<Statement, lombok.ast.Block> astContents() {
		return this.contents.asStrict();
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		result.addAll(this.contents.backingList());
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.rawContents().replace(original, replacement)) return true;
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.rawContents().remove(child)) return true;
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitBlock(this)) return;
		for (lombok.ast.Node child : this.contents.asIterable()) {
			child.accept(visitor);
		}
		visitor.afterVisitBlock(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public Block copy() {
		Block result = new Block();
		for (Node n : this.contents.backingList()) {
			result.rawContents().addToEnd(n == null ? null : n.copy());
		}
		return result;
	}
	
}
