//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class ClassLiteral extends lombok.ast.AbstractNode implements lombok.ast.Expression, lombok.ast.DescribedNode {
	private java.util.List<lombok.ast.Position> parensPositions = new java.util.ArrayList<lombok.ast.Position>();
	private lombok.ast.AbstractNode typeReference = null;
	
	public java.util.List<lombok.ast.Position> astParensPositions() {
		return this.parensPositions;
	}
	
	public TypeReference astTypeReference() {
		if (!(this.typeReference instanceof TypeReference)) return null;
		return (TypeReference) this.typeReference;
	}
	
	public lombok.ast.ClassLiteral astTypeReference(TypeReference typeReference) {
		if (typeReference == null) throw new java.lang.NullPointerException("typeReference is mandatory");
		return this.rawTypeReference(typeReference);
	}
	
	public lombok.ast.Node rawTypeReference() {
		return this.typeReference;
	}
	
	public lombok.ast.ClassLiteral rawTypeReference(lombok.ast.Node typeReference) {
		if (typeReference == this.typeReference) return this;
		if (typeReference != null) this.adopt((lombok.ast.AbstractNode)typeReference);
		if (this.typeReference != null) this.disown(this.typeReference);
		this.typeReference = (lombok.ast.AbstractNode)typeReference;
		return this;
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		if (this.typeReference != null) result.add(this.typeReference);
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.typeReference == original) {
			this.rawTypeReference(replacement);
			return true;
		}
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.typeReference == child) {
			this.disown((AbstractNode) child);
			this.typeReference = null;
			return true;
		}
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitClassLiteral(this)) return;
		if (this.typeReference != null) this.typeReference.accept(visitor);
		visitor.afterVisitClassLiteral(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public ClassLiteral copy() {
		ClassLiteral result = new ClassLiteral();
		result.parensPositions = new java.util.ArrayList<lombok.ast.Position>(this.parensPositions);
		if (this.typeReference != null) result.rawTypeReference(this.typeReference.copy());
		return result;
	}
	
	public java.lang.String getDescription() {
		return lombok.ast.ClassLiteralTemplate.getDescription(this);
	}
	
	public int getParens() {
		return lombok.ast.ExpressionMixin.getParens(this);
	}
	
	public int getIntendedParens() {
		return lombok.ast.ExpressionMixin.getIntendedParens(this);
	}
	
	public boolean needsParentheses() {
		return lombok.ast.ExpressionMixin.needsParentheses(this);
	}
	
	public boolean isStatementExpression() {
		return lombok.ast.ExpressionMixin.isStatementExpression(this);
	}
	
}
