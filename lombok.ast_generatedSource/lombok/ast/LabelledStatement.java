//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class LabelledStatement extends lombok.ast.AbstractNode implements lombok.ast.Statement, lombok.ast.DescribedNode {
	private lombok.ast.AbstractNode label = adopt(new lombok.ast.Identifier());
	private lombok.ast.AbstractNode statement = null;
	
	public Block upToBlock() {
		if (!(this.getParent() instanceof Block)) return null;
		Block out = (Block)this.getParent();
		if (!out.rawContents().contains(this)) return null;
		return out;
	}
	
	
	public Identifier astLabel() {
		if (!(this.label instanceof Identifier)) return null;
		return (Identifier) this.label;
	}
	
	public lombok.ast.LabelledStatement astLabel(Identifier label) {
		return this.rawLabel(label);
	}
	
	private lombok.ast.LabelledStatement rawLabel(lombok.ast.Node label) {
		if (label == this.label) return this;
		if (label != null) this.adopt((lombok.ast.AbstractNode)label);
		if (this.label != null) this.disown(this.label);
		this.label = (lombok.ast.AbstractNode)label;
		return this;
	}
	
	public Statement astStatement() {
		if (!(this.statement instanceof Statement)) return null;
		return (Statement) this.statement;
	}
	
	public lombok.ast.LabelledStatement astStatement(Statement statement) {
		if (statement == null) throw new java.lang.NullPointerException("statement is mandatory");
		return this.rawStatement(statement);
	}
	
	public lombok.ast.Node rawStatement() {
		return this.statement;
	}
	
	public lombok.ast.LabelledStatement rawStatement(lombok.ast.Node statement) {
		if (statement == this.statement) return this;
		if (statement != null) this.adopt((lombok.ast.AbstractNode)statement);
		if (this.statement != null) this.disown(this.statement);
		this.statement = (lombok.ast.AbstractNode)statement;
		return this;
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		if (this.label != null) result.add(this.label);
		if (this.statement != null) result.add(this.statement);
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.label == original) {
			if (replacement instanceof Identifier) {
				this.astLabel((Identifier) replacement);
				return true;
			} else throw new lombok.ast.AstException(this, String.format(
					"Cannot replace node: replacement must be of type %s but is of type %s",
					"Identifier", replacement == null ? "null" : replacement.getClass().getName()));
		}
		if (this.statement == original) {
			this.rawStatement(replacement);
			return true;
		}
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.label == child) {
			this.disown((AbstractNode) child);
			this.label = null;
			return true;
		}
		if (this.statement == child) {
			this.disown((AbstractNode) child);
			this.statement = null;
			return true;
		}
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitLabelledStatement(this)) return;
		if (this.label != null) this.label.accept(visitor);
		if (this.statement != null) this.statement.accept(visitor);
		visitor.afterVisitLabelledStatement(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public LabelledStatement copy() {
		LabelledStatement result = new LabelledStatement();
		if (this.label != null) result.rawLabel(this.label.copy());
		if (this.statement != null) result.rawStatement(this.statement.copy());
		return result;
	}
	
	public java.lang.String getDescription() {
		return lombok.ast.LabelledStatementTemplate.getDescription(this);
	}
	
}
