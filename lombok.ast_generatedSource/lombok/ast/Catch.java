//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class Catch extends lombok.ast.AbstractNode implements lombok.ast.DescribedNode {
	private lombok.ast.AbstractNode exceptionDeclaration = null;
	private lombok.ast.AbstractNode body = null;
	
	public Try upToTry() {
		if (!(this.getParent() instanceof Try)) return null;
		Try out = (Try)this.getParent();
		if (!out.rawCatches().contains(this)) return null;
		return out;
	}
	
	
	public VariableDefinition astExceptionDeclaration() {
		if (!(this.exceptionDeclaration instanceof VariableDefinition)) return null;
		return (VariableDefinition) this.exceptionDeclaration;
	}
	
	public lombok.ast.Catch astExceptionDeclaration(VariableDefinition exceptionDeclaration) {
		if (exceptionDeclaration == null) throw new java.lang.NullPointerException("exceptionDeclaration is mandatory");
		return this.rawExceptionDeclaration(exceptionDeclaration);
	}
	
	public lombok.ast.Node rawExceptionDeclaration() {
		return this.exceptionDeclaration;
	}
	
	public lombok.ast.Catch rawExceptionDeclaration(lombok.ast.Node exceptionDeclaration) {
		if (exceptionDeclaration == this.exceptionDeclaration) return this;
		if (exceptionDeclaration != null) this.adopt((lombok.ast.AbstractNode)exceptionDeclaration);
		if (this.exceptionDeclaration != null) this.disown(this.exceptionDeclaration);
		this.exceptionDeclaration = (lombok.ast.AbstractNode)exceptionDeclaration;
		return this;
	}
	
	public Block astBody() {
		if (!(this.body instanceof Block)) return null;
		return (Block) this.body;
	}
	
	public lombok.ast.Catch astBody(Block body) {
		if (body == null) throw new java.lang.NullPointerException("body is mandatory");
		return this.rawBody(body);
	}
	
	public lombok.ast.Node rawBody() {
		return this.body;
	}
	
	public lombok.ast.Catch rawBody(lombok.ast.Node body) {
		if (body == this.body) return this;
		if (body != null) this.adopt((lombok.ast.AbstractNode)body);
		if (this.body != null) this.disown(this.body);
		this.body = (lombok.ast.AbstractNode)body;
		return this;
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		if (this.exceptionDeclaration != null) result.add(this.exceptionDeclaration);
		if (this.body != null) result.add(this.body);
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.exceptionDeclaration == original) {
			this.rawExceptionDeclaration(replacement);
			return true;
		}
		if (this.body == original) {
			this.rawBody(replacement);
			return true;
		}
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.exceptionDeclaration == child) {
			this.disown((AbstractNode) child);
			this.exceptionDeclaration = null;
			return true;
		}
		if (this.body == child) {
			this.disown((AbstractNode) child);
			this.body = null;
			return true;
		}
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitCatch(this)) return;
		if (this.exceptionDeclaration != null) this.exceptionDeclaration.accept(visitor);
		if (this.body != null) this.body.accept(visitor);
		visitor.afterVisitCatch(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public Catch copy() {
		Catch result = new Catch();
		if (this.exceptionDeclaration != null) result.rawExceptionDeclaration(this.exceptionDeclaration.copy());
		if (this.body != null) result.rawBody(this.body.copy());
		return result;
	}
	
	public java.lang.String getDescription() {
		return lombok.ast.CatchTemplate.getDescription(this);
	}
	
}
