//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class ArrayInitializer extends lombok.ast.AbstractNode implements lombok.ast.Expression {
	private java.util.List<lombok.ast.Position> parensPositions = new java.util.ArrayList<lombok.ast.Position>();
	lombok.ast.ListAccessor<Expression, lombok.ast.ArrayInitializer> expressions = ListAccessor.of(this, Expression.class, "ArrayInitializer.expressions");
	
	public ArrayCreation upToArrayCreation() {
		if (!(this.getParent() instanceof ArrayCreation)) return null;
		ArrayCreation out = (ArrayCreation)this.getParent();
		if (out.rawInitializer() != this) return null;
		return out;
	}
	
	
	public java.util.List<lombok.ast.Position> astParensPositions() {
		return this.parensPositions;
	}
	
	public lombok.ast.RawListAccessor<Expression, lombok.ast.ArrayInitializer> rawExpressions() {
		return this.expressions.asRaw();
	}
	
	public lombok.ast.StrictListAccessor<Expression, lombok.ast.ArrayInitializer> astExpressions() {
		return this.expressions.asStrict();
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		result.addAll(this.expressions.backingList());
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.rawExpressions().replace(original, replacement)) return true;
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.rawExpressions().remove(child)) return true;
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitArrayInitializer(this)) return;
		for (lombok.ast.Node child : this.expressions.asIterable()) {
			child.accept(visitor);
		}
		visitor.afterVisitArrayInitializer(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public ArrayInitializer copy() {
		ArrayInitializer result = new ArrayInitializer();
		result.parensPositions = new java.util.ArrayList<lombok.ast.Position>(this.parensPositions);
		for (Node n : this.expressions.backingList()) {
			result.rawExpressions().addToEnd(n == null ? null : n.copy());
		}
		return result;
	}
	
	public int getParens() {
		return lombok.ast.ExpressionMixin.getParens(this);
	}
	
	public int getIntendedParens() {
		return lombok.ast.ExpressionMixin.getIntendedParens(this);
	}
	
	public boolean needsParentheses() {
		return lombok.ast.ExpressionMixin.needsParentheses(this);
	}
	
	public boolean isStatementExpression() {
		return lombok.ast.ExpressionMixin.isStatementExpression(this);
	}
	
}
