//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class AnnotationValueArray extends lombok.ast.AbstractNode implements lombok.ast.AnnotationValue {
	lombok.ast.ListAccessor<AnnotationValue, lombok.ast.AnnotationValueArray> values = ListAccessor.of(this, AnnotationValue.class, "AnnotationValueArray");
	
	public lombok.ast.RawListAccessor<AnnotationValue, lombok.ast.AnnotationValueArray> rawValues() {
		return this.values.asRaw();
	}
	
	public lombok.ast.StrictListAccessor<AnnotationValue, lombok.ast.AnnotationValueArray> astValues() {
		return this.values.asStrict();
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		result.addAll(this.values.backingList());
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.rawValues().replace(original, replacement)) return true;
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.rawValues().remove(child)) return true;
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitAnnotationValueArray(this)) return;
		for (lombok.ast.Node child : this.values.asIterable()) {
			child.accept(visitor);
		}
		visitor.afterVisitAnnotationValueArray(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public AnnotationValueArray copy() {
		AnnotationValueArray result = new AnnotationValueArray();
		for (Node n : this.values.backingList()) {
			result.rawValues().addToEnd(n == null ? null : n.copy());
		}
		return result;
	}
	
}
