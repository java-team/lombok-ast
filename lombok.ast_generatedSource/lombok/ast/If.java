//Generated by lombok.ast.template.TemplateProcessor. DO NOT EDIT, DO NOT CHECK IN!

package lombok.ast;

public class If extends lombok.ast.AbstractNode implements lombok.ast.Statement {
	private lombok.ast.AbstractNode condition = null;
	private lombok.ast.AbstractNode statement = null;
	private lombok.ast.AbstractNode elseStatement = null;
	
	public Block upToBlock() {
		if (!(this.getParent() instanceof Block)) return null;
		Block out = (Block)this.getParent();
		if (!out.rawContents().contains(this)) return null;
		return out;
	}
	
	
	public Expression astCondition() {
		if (!(this.condition instanceof Expression)) return null;
		return (Expression) this.condition;
	}
	
	public lombok.ast.If astCondition(Expression condition) {
		if (condition == null) throw new java.lang.NullPointerException("condition is mandatory");
		return this.rawCondition(condition);
	}
	
	public lombok.ast.Node rawCondition() {
		return this.condition;
	}
	
	public lombok.ast.If rawCondition(lombok.ast.Node condition) {
		if (condition == this.condition) return this;
		if (condition != null) this.adopt((lombok.ast.AbstractNode)condition);
		if (this.condition != null) this.disown(this.condition);
		this.condition = (lombok.ast.AbstractNode)condition;
		return this;
	}
	
	public Statement astStatement() {
		if (!(this.statement instanceof Statement)) return null;
		return (Statement) this.statement;
	}
	
	public lombok.ast.If astStatement(Statement statement) {
		if (statement == null) throw new java.lang.NullPointerException("statement is mandatory");
		return this.rawStatement(statement);
	}
	
	public lombok.ast.Node rawStatement() {
		return this.statement;
	}
	
	public lombok.ast.If rawStatement(lombok.ast.Node statement) {
		if (statement == this.statement) return this;
		if (statement != null) this.adopt((lombok.ast.AbstractNode)statement);
		if (this.statement != null) this.disown(this.statement);
		this.statement = (lombok.ast.AbstractNode)statement;
		return this;
	}
	
	public Statement astElseStatement() {
		if (!(this.elseStatement instanceof Statement)) return null;
		return (Statement) this.elseStatement;
	}
	
	public lombok.ast.If astElseStatement(Statement elseStatement) {
		return this.rawElseStatement(elseStatement);
	}
	
	public lombok.ast.Node rawElseStatement() {
		return this.elseStatement;
	}
	
	public lombok.ast.If rawElseStatement(lombok.ast.Node elseStatement) {
		if (elseStatement == this.elseStatement) return this;
		if (elseStatement != null) this.adopt((lombok.ast.AbstractNode)elseStatement);
		if (this.elseStatement != null) this.disown(this.elseStatement);
		this.elseStatement = (lombok.ast.AbstractNode)elseStatement;
		return this;
	}
	
	@java.lang.Override public java.util.List<Node> getChildren() {
		java.util.List<Node> result = new java.util.ArrayList<Node>();
		if (this.condition != null) result.add(this.condition);
		if (this.statement != null) result.add(this.statement);
		if (this.elseStatement != null) result.add(this.elseStatement);
		return result;
	}
	
	@java.lang.Override public boolean replaceChild(Node original, Node replacement) throws lombok.ast.AstException {
		if (this.condition == original) {
			this.rawCondition(replacement);
			return true;
		}
		if (this.statement == original) {
			this.rawStatement(replacement);
			return true;
		}
		if (this.elseStatement == original) {
			this.rawElseStatement(replacement);
			return true;
		}
		return false;
	}
	
	@java.lang.Override public boolean detach(Node child) {
		if (this.condition == child) {
			this.disown((AbstractNode) child);
			this.condition = null;
			return true;
		}
		if (this.statement == child) {
			this.disown((AbstractNode) child);
			this.statement = null;
			return true;
		}
		if (this.elseStatement == child) {
			this.disown((AbstractNode) child);
			this.elseStatement = null;
			return true;
		}
		return false;
	}
	
	@java.lang.Override public void accept(lombok.ast.AstVisitor visitor) {
		if (visitor.visitIf(this)) return;
		if (this.condition != null) this.condition.accept(visitor);
		if (this.statement != null) this.statement.accept(visitor);
		if (this.elseStatement != null) this.elseStatement.accept(visitor);
		visitor.afterVisitIf(this);
		visitor.endVisit(this);
	}
	
	@java.lang.Override public If copy() {
		If result = new If();
		if (this.condition != null) result.rawCondition(this.condition.copy());
		if (this.statement != null) result.rawStatement(this.statement.copy());
		if (this.elseStatement != null) result.rawElseStatement(this.elseStatement.copy());
		return result;
	}
	
}
